#FROM drogonframework/drogon
FROM goodrain.me/drogon

ADD ./app/helloworld /drogon/app/helloworld
RUN  cd /drogon/app/helloworld && \
cmake ./ && make

WORKDIR /drogon/app/helloworld
RUN echo '<h1>Hello world!</h1>' >>index.html
EXPOSE 80
CMD ./hello_world